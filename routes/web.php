<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes([
    'register' => false
]);

//Route::group(['middleware' => 'auth'], function () {
    
    Route::get('/', function () {
        return redirect('companies');
    });

    Route::resource('companies', 'CompanyController')->only([
        'index', 'edit', 'create'
    ]);
    Route::resource('employees', 'EmployeeController')->only([
        'index', 'edit', 'create'
    ]);
//});
