<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('companies/list', 'CompanyController@list');
Route::post('companies/upload-logo', 'CompanyController@uploadLogo');
Route::apiResource('companies', 'CompanyController');

Route::get('employees/list', 'EmployeeController@list');
Route::apiResource('employees', 'EmployeeController');
