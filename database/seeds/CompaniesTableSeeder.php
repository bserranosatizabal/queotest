<?php

use Illuminate\Database\Seeder;
use App\Company;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
 
        // Create 50 companies records
        for ($i = 0; $i < 3; $i++) {
            Company::create([
                'name' => $faker->company,
                'email' => $faker->email,
                'logo' => $faker->imageUrl($width = 100, $height = 100),
                'website' => $faker->domainName
            ]);
        }
    }
}
