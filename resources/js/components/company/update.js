import React, {Component} from 'react';
import axios from 'axios';
import ReactDOM from 'react-dom';
import ImageUploader from 'react-images-upload';
import MyGlobleSetting from '../MyGlobleSetting';


class CompanyUpdate extends Component {
  constructor(props) {
      super(props);
      this.onDrop = this.onDrop.bind(this);
      this.state = {id: '', logo: '', name: '', email: '', website: ''};
      this.handleChange1 = this.handleChange1.bind(this);
      this.handleChange2 = this.handleChange2.bind(this);
      this.handleChange3 = this.handleChange3.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
  }


  componentDidMount(){
    
    axios.get(MyGlobleSetting.url + `/api/companies/${companyId}`)
    .then(response => {
      this.setState({ 
        id: response.data.id,
        logo: response.data.logo,
        name: response.data.name, 
        email: response.data.email, 
        website: response.data.website
      });
    })
    .catch(function (error) {
      console.log(error);
    })
  }

  onDrop(picture) {
    let uri = MyGlobleSetting.url + '/api/companies/upload-logo';
    var formData = new FormData();
    formData.append("logo", picture[0]);
    
    axios.post('/api/companies/upload-logo', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
    }).then((response) => {
        this.setState({
          logo: response.data.logo,
        });
    });
  }

  handleChange1(e){
    this.setState({
      name: e.target.value
    })
  }
  
  handleChange2(e){
    this.setState({
      email: e.target.value
    })
  }

  handleChange3(e){
    this.setState({
      website: e.target.value
    })
  }

  handleSubmit(event) {
    event.preventDefault();
    const company = {
      id: this.state.id,
      logo: this.state.logo,
      name: this.state.name,
      email: this.state.email,
      website: this.state.website
    }
    let uri = MyGlobleSetting.url + '/api/companies/'+this.state.id;
    axios.patch(uri, company).then((response) => {
        console.log('updated');
        window.location.href = '/companies';
    });
  }
  render(){
    return (
        <div>
          <form onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="col-md-6">
                <ImageUploader
                    withIcon={true} 
                    withPreview={true} 
                    buttonText='Cargar logo'
                    onChange={this.onDrop}
                    imgExtension={['.jpg', '.gif', '.png', '.gif']}
                    maxFileSize={5242880} 
                    label="Tamaño máximo: 5mb, Se aceptan: jpg|gif|png" 
                    singleImage={true}
                />
                <img src={this.state.logo}/>
              </div>
              <div className="col-md-6">
                <div className="row">
                    <div className="col">
                        <div className="form-group">
                            <label>Nombre de la empresa:</label>
                            <input type="text" className="form-control" value={this.state.name} onChange={this.handleChange1} />
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col">
                        <div className="form-group">
                            <label>correo electrónico:</label>
                            <input type="email" className="form-control" value={this.state.email} onChange={this.handleChange2} />
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col">
                        <div className="form-group">
                            <label>Sitio web:</label>
                            <input type="text" className="form-control" value={this.state.website} onChange={this.handleChange3} />
                        </div>
                    </div>
                </div><br />
                <div className="form-group">
                  <button type="submit" className="btn btn-primary">Actualizar Empresa</button>
                </div>
              </div>
            </div>
        </form>
    </div>
    )
  }
}
export default CompanyUpdate;

/* The if statement is required so as to Render the component on pages that have a div with an ID of "root";  
*/
 
if (document.getElementById('company-update')) {
  ReactDOM.render(<CompanyUpdate />, document.getElementById('company-update'));
}