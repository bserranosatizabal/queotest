import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import ImageUploader from 'react-images-upload';
import MyGlobleSetting from '../MyGlobleSetting';


class CompanyCreate extends Component {
  constructor(props){
    super(props);
    this.state = {companyLogo: '', companyName: '', companyEmail: '', companyWebsite: '', errorMessage: ''};

    this.onDrop = this.onDrop.bind(this);
    this.handleChange1 = this.handleChange1.bind(this);
    this.handleChange2 = this.handleChange2.bind(this);
    this.handleChange3 = this.handleChange3.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);


  }
  handleChange1(e){
    this.setState({
      companyName: e.target.value
    })
  }
  handleChange2(e){
    this.setState({
      companyEmail: e.target.value
    })
  }

  handleChange3(e){
    this.setState({
      companyWebsite: e.target.value
    })
  }

  handleSubmit(e){
    e.preventDefault();
    const company = {
      logo: this.state.companyLogo,
      name: this.state.companyName,
      email: this.state.companyEmail,
      website: this.state.companyWebsite
    }
    let uri = MyGlobleSetting.url + '/api/companies';
    axios.post(uri, company).then((response) => {
      window.location.href = '/companies';
    }).catch(err => console.log(err.response));
  }

  onDrop(picture) {
    let uri = MyGlobleSetting.url + '/api/companies/upload-logo';
    var formData = new FormData();
    formData.append("logo", picture[0]);
    
    axios.post('/api/companies/upload-logo', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
    }).then((response) => {
        this.setState({
          companyLogo: response.data.logo,
        });
    });
  }


    render() {
      const errorMessage = this.state.errorMessage;
      let message;
      if (errorMessage) {
        message = <div class="alert alert-danger" role="alert">{errorMessage}</div>;
      }

      return (
        <div>
          {message}
            <form onSubmit={this.handleSubmit}>
                <div className="row">
                  <div className="col-md-6">
                    <ImageUploader
                        withIcon={true} 
                        withPreview={true} 
                        buttonText='Cargar logo'
                        onChange={this.onDrop}
                        imgExtension={['.jpg', '.gif', '.png', '.gif']}
                        maxFileSize={5242880} 
                        label="Tamaño máximo: 5mb, Se aceptan: jpg|gif|png" 
                        singleImage={true}
                    />
                  </div>
                  <div className="col-md-6">
                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <label>Nombre de la empresa:</label>
                                <input type="text" className="form-control" onChange={this.handleChange1} />
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <label>correo electrónico:</label>
                                <input type="email" className="form-control" onChange={this.handleChange2} />
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <label>Sitio web:</label>
                                <input type="text" className="form-control" onChange={this.handleChange3} />
                            </div>
                        </div>
                    </div><br />
                    <div className="form-group">
                      <button className="btn btn-primary">Crear Empresa</button>
                    </div>
                  </div>
                </div>
            </form>
        </div>
      )
    }
}
export default CompanyCreate;

/* The if statement is required so as to Render the component on pages that have a div with an ID of "root";  
*/
 
if (document.getElementById('root')) {
  ReactDOM.render(<CompanyCreate />, document.getElementById('root'));
}