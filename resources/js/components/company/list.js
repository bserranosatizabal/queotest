import React, { Component } from 'react';
import ReactDOM from 'react-dom';
 
/* An example React component */
class CompanyList extends Component {
    constructor() {
   
        super();
        //Initialize the state in the constructor
        this.state = {
            companies: [],
        }
      }
      /*componentDidMount() is a lifecycle method
       * that gets called after the component is rendered
       */
      componentDidMount() {
    
        this.getList();

      }

      getList(){
        fetch('/api/companies/list')
        .then(response => {
            return response.json();
        })
        .then(companies => {
            //Fetched company is stored in the state
            this.setState({ companies });

            $(document).ready( function () {
                $('#table_id').DataTable();
            });
        });
      }


      remove(id){
        axios.delete('api/companies/' + id).then((response) => {
            this.getList();
        });
      }
     
     renderCompanies() {
        return this.state.companies.map(company => {
            return (
                /* When using list you need to specify a key
                 * attribute that is unique for each list item
                */
               <tr key={company.id}>
                    <td><img src={company.logo}/></td>
                    <td>{ company.name }</td>
                    <td>{ company.email }</td>
                    <td>{ company.website }</td>
                    <td>
                    <div className="row">
                        <div className="col-xs-12 col-sm-6 col-md-4">
                            <a href={"/companies/" + company.id +"/edit"} className="btn btn-primary">Editar</a>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-4">
                            <button type="button" className="btn btn-danger" onClick={() =>this.remove(company.id)}>Remover</button>
                        </div>
                    </div>
                    </td>
                </tr>     
            );
        })
      }
       
      render() {
       /* Some css code has been removed for brevity */
        return (
            <div>
            <a href="/companies/create" className="btn btn-primary">Crear Empresa</a><br/><br/>
            <table id="table_id">
                <thead>
                    <tr>
                        <th>Logo</th>
                        <th>Nombre de la empresa</th>
                        <th>Correo electrónico</th>
                        <th>Sitio web</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    { this.renderCompanies() }
                </tbody>
            </table>
            </div>
           
        );
      }
}
 
export default CompanyList;
 
/* The if statement is required so as to Render the component on pages that have a div with an ID of "root";  
*/
 
if (document.getElementById('company-list')) {
    ReactDOM.render(<CompanyList />, document.getElementById('company-list'));
}