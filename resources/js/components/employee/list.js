import React, { Component } from 'react';
import ReactDOM from 'react-dom';
 
/* An example React component */
class EmployeeList extends Component {
    constructor() {
   
        super();
        //Initialize the state in the constructor
        this.state = {
            employees: [],
        }
      }
      /*componentDidMount() is a lifecycle method
       * that gets called after the component is rendered
       */
      componentDidMount() {
    
        this.getList();

      }

      getList(){
        fetch('/api/employees/list')
        .then(response => {
            return response.json();
        })
        .then(employees => {
            console.log(employees);
            
            //Fetched employees is stored in the state
            this.setState({ employees });

            $(document).ready( function () {
                $('#employee_table').DataTable();
            });
        });
      }


      remove(id){
        axios.delete('api/employees/' + id).then((response) => {
            this.getList();
        });
      }
     
     renderEmployees() {
        return this.state.employees.map(employee => {
            return (
                /* When using list you need to specify a key
                 * attribute that is unique for each list item
                */
               <tr key={employee.id}>
                    <td>{employee.first_name + ' ' + employee.last_name }</td>
                    <td>{ employee.company.name }</td>
                    <td>{ employee.email }</td>
                    <td>{ employee.phone }</td>
                    <td>
                        <div className="row">
                            <div className="col-xs-12 col-3">
                                <a href={"/employees/" + employee.id +"/edit"} className="btn btn-primary">Editar</a>
                            </div>
                            <div className="col-xs-12 col-3">
                                <button type="button" className="btn btn-danger" onClick={() =>this.remove(employee.id)}>Remover</button>
                            </div>
                        </div>
                    </td>
                </tr>     
            );
        })
      }
       
      render() {
       /* Some css code has been removed for brevity */
        return (
            <div>
            <a href="/employees/create" className="btn btn-primary">Crear Empleado</a><br/><br/>
            <table id="employee_table">
                <thead>
                    <tr>
                        <th>Nombre y apellido</th>
                        <th>Nombre de la empresa</th>
                        <th>Correo electrónico</th>
                        <th>Teléfono</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    { this.renderEmployees() }
                </tbody>
            </table>
            </div>
           
        );
      }
}
 
export default EmployeeList;
 
/* The if statement is required so as to Render the component on pages that have a div with an ID of "root";  
*/
 
if (document.getElementById('employee-list')) {
    ReactDOM.render(<EmployeeList />, document.getElementById('employee-list'));
}