import React, {Component} from 'react';
import axios from 'axios';
import ReactDOM from 'react-dom';
import MyGlobleSetting from '../MyGlobleSetting';


class EmployeeUpdate extends Component {
  constructor(props) {
      super(props);
      this.state = {id: '', first_name: '', last_name: '', company_id: '', email: '', phone: '', companies: []};
      this.handleChange1 = this.handleChange1.bind(this);
      this.handleChange2 = this.handleChange2.bind(this);
      this.handleChange3 = this.handleChange3.bind(this);
      this.handleChange4 = this.handleChange4.bind(this);
      this.handleChange5 = this.handleChange5.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
  }


  componentDidMount(){
    this.getCompanies();
    axios.get(MyGlobleSetting.url + `/api/employees/${employeeId}`)
    .then(response => {
      this.setState({ 
        id: response.data.id,
        first_name: response.data.first_name,
        last_name: response.data.last_name, 
        company_id: response.data.company.id,
        email: response.data.email, 
        phone: response.data.phone
      });
    })
    .catch(function (error) {
      console.log(error);
    })
  }

  getCompanies(){
    fetch('/api/companies/list')
    .then(response => {
        return response.json();
    })
    .then(companies => {
        //Fetched company is stored in the state
        this.setState({ companies });
    });
  }

  handleChange1(e){
    this.setState({
      first_name: e.target.value
    })
  }
  
  handleChange2(e){
    this.setState({
      last_name: e.target.value
    })
  }

  handleChange3(e){
    this.setState({
      company_id: e.target.value
    })
  }

  handleChange4(e){
    this.setState({
      email: e.target.value
    })
  }

  handleChange5(e){
    this.setState({
      phone: e.target.value
    })
  }

  handleSubmit(event) {
    event.preventDefault();
    const employee = {
      id: this.state.id,
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      company_id: this.state.company_id,
      email: this.state.email,
      phone: this.state.phone
    }
    let uri = MyGlobleSetting.url + '/api/employees/'+this.state.id;
    axios.patch(uri, employee).then((response) => {
        console.log('updated');
        window.location.href = '/employees';
    });
  }

  renderCompanies(){
    return this.state.companies.map(company => {
      return (
        /* When using list you need to specify a key
          * attribute that is unique for each list item
        */
        <option key={company.id} value={company.id}>{ company.name }</option>    
      );
    })
  }

  render(){
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="col-md-8">
                <div className="row">
                    <div className="col">
                        <div className="form-group">
                            <label>Nombre:</label>
                            <input type="text" className="form-control" value={this.state.first_name} onChange={this.handleChange1} />
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col">
                        <div className="form-group">
                            <label>Apellido:</label>
                            <input type="text" className="form-control" value={this.state.last_name} onChange={this.handleChange2} />
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col">
                        <div className="form-group">
                            <label>Empresa:</label>
                            <select className="form-control" value={this.state.company_id} onChange={this.handleChange3}>
                              { this.renderCompanies() }
                            </select>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col">
                        <div className="form-group">
                            <label>Email:</label>
                            <input type="email" className="form-control" value={this.state.email} onChange={this.handleChange4} />
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col">
                        <div className="form-group">
                            <label>Teléfono:</label>
                            <input type="text" className="form-control" value={this.state.phone} onChange={this.handleChange5} />
                        </div>
                    </div>
                </div><br />
                <div className="form-group">
                  <button className="btn btn-primary">Actualizar Empleado</button>
                </div>
              </div>
            </div>
        </form>
    </div>
    )
  }
}
export default EmployeeUpdate;

/* The if statement is required so as to Render the component on pages that have a div with an ID of "root";  
*/
 
if (document.getElementById('employee-update')) {
  ReactDOM.render(<EmployeeUpdate />, document.getElementById('employee-update'));
}