import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import MyGlobleSetting from '../MyGlobleSetting';


class EmployeeCreate extends Component {
  constructor(props){
    super(props);
    this.state = { employeeFirstName: '', employeeLastName: '', employeeCompanyId: '', employeeEmail: '', employeePhone: '', companies: []};
    this.handleChange1 = this.handleChange1.bind(this);
    this.handleChange2 = this.handleChange2.bind(this);
    this.handleChange3 = this.handleChange3.bind(this);
    this.handleChange4 = this.handleChange4.bind(this);
    this.handleChange5 = this.handleChange5.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);


  }

  componentDidMount() {
    
    this.getCompanies();

  }

  handleChange1(e){
    this.setState({
      employeeFirstName: e.target.value
    })
  }
  handleChange2(e){
    this.setState({
      employeeLastName: e.target.value
    })
  }

  handleChange3(e){
    this.setState({
      employeeCompanyId: e.target.value
    })
  }

  handleChange4(e){
    this.setState({
      employeeEmail: e.target.value
    })
  }

  handleChange5(e){
    this.setState({
      employeePhone: e.target.value
    })
  }

  handleSubmit(e){
    e.preventDefault();
    const employee = {
      first_name: this.state.employeeFirstName,
      last_name: this.state.employeeLastName,
      company_id: this.state.employeeCompanyId,
      email: this.state.employeeEmail,
      phone: this.state.employeePhone
    }
    let uri = MyGlobleSetting.url + '/api/employees';
    axios.post(uri, employee).then((response) => {
      window.location.href = '/employees';
    });
  }

  getCompanies(){
    fetch('/api/companies/list')
    .then(response => {
        return response.json();
    })
    .then(companies => {
        //Fetched company is stored in the state
        this.setState({ companies });
        this.setState({
          employeeCompanyId: companies[0].id
        });
    });
  }

  renderCompanies(){
    return this.state.companies.map(company => {
      return (
        /* When using list you need to specify a key
          * attribute that is unique for each list item
        */
        <option key={company.id} value={company.id}>{ company.name }</option>    
      );
  })
  }


    render() {
      return (
        <div>
            <form onSubmit={this.handleSubmit}>
                <div className="row">
                  <div className="col-md-8">
                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <label>Nombre:</label>
                                <input type="text" className="form-control" onChange={this.handleChange1} />
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <label>Apellido:</label>
                                <input type="text" className="form-control" onChange={this.handleChange2} />
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <label>Empresa:</label>
                                <select value={this.state.employeeCompanyId} className="form-control" onChange={this.handleChange3}>
                                  { this.renderCompanies() }
                                </select>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <label>Email:</label>
                                <input type="email" className="form-control" onChange={this.handleChange4} />
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <label>Teléfono:</label>
                                <input type="text" className="form-control" onChange={this.handleChange5} />
                            </div>
                        </div>
                    </div><br />
                    <div className="form-group">
                      <button className="btn btn-primary">Crear Empresa</button>
                    </div>
                  </div>
                </div>
            </form>
        </div>
      )
    }
}
export default EmployeeCreate;

/* The if statement is required so as to Render the component on pages that have a div with an ID of "root";  
*/
 
if (document.getElementById('create-employee')) {
  ReactDOM.render(<EmployeeCreate />, document.getElementById('create-employee'));
}