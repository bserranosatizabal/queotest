/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/* Import the Company components */
import CompanyList from './components/company/list';
import CompanyCreate from './components/company/create';
import CompanyUpdate from './components/company/update';

/* Import the Employee components */
import EmployeeList from './components/employee/list';
import EmployeeCreate from './components/employee/create';
import EmployeeUpdate from './components/employee/update';
