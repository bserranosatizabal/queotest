@extends('adminlte::page')

@section('title', 'Editar Empleado')

@section('content_header')
    <h1>Editar Empleado</h1>
@stop

@section('content')
    <div id="employee-update"></div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>employeeId = {!! json_encode($employeeId) !!};</script>
    <script src="{{asset('js/app.js')}}" ></script>
@stop