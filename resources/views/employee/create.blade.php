@extends('adminlte::page')

@section('title', 'Crear Empleado')

@section('content_header')
    <h1>Crear Empleado</h1>
@stop

@section('content')
    <div id="create-employee"></div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script src="{{asset('js/app.js')}}" ></script>
@stop