@extends('adminlte::page')

@section('title', 'Editar Empresa')

@section('content_header')
    <h1>Editar Empresa</h1>
@stop

@section('content')
    <div id="company-update"></div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>companyId = {!! json_encode($companyId) !!};</script>
    <script src="{{asset('js/app.js')}}" ></script>
@stop