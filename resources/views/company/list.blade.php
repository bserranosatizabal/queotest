@extends('adminlte::page')

@section('title', 'Empresas')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div id="company-list"></div>
@stop

@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <script src="{{asset('js/app.js')}}" ></script>
@stop