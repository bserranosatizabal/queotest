@extends('adminlte::page')

@section('title', 'Crear Empresa')

@section('content_header')
    <h1>Crear Empresa</h1>
@stop

@section('content')
    <div id="root"></div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script src="{{asset('js/app.js')}}" ></script>
@stop