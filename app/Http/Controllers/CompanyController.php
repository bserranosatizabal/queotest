<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Traits\UploadTrait;
use App\Company;

class CompanyController extends Controller
{
    use UploadTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('company.list');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $companies = Company::all();
        return response()->json($companies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Form validation
        $request->validate([
            'logo'     =>  'required',
            'name'     =>  'required',
            'email'     =>  'required|email',
            'website'     =>  'required',
        ]);


        $company = new Company([
            'logo' => $request->get('logo'),
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'website' => $request->get('website'),
        ]);

        $company->save();
  
        return response()->json('Company added succesfully.');
    }


    /**
     * Store the company logo
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function uploadLogo(Request $request){
        // Form validation
        $request->validate([
            'logo'     =>  'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        if ($request->has('logo')) {
            // Get image file
            $image = $request->file('logo');
            // Make a image name based on user name and current timestamp
            $name = Carbon::now()->timestamp . '_' . uniqid() . '.';
            // Define folder path
            $folder = '/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            return response()->json([
                'logo' => asset('storage/' . $name . $image->getClientOriginalExtension())
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);
        return response()->json($company);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('company.edit', ['companyId' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Form validation
        $request->validate([
            'logo'     =>  'required',
            'name'     =>  'required',
            'email'     =>  'required|email',
            'website'     =>  'required',
        ]);

        $company = Company::find($id);
        $company->name = $request->get('name');
        $company->email = $request->get('email');
        $company->logo = $request->get('logo');
        $company->website = $request->get('website');
        $company->save();


        return response()->json('Company Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        $company->delete();


        return response()->json('Company Deleted Successfully.');
    }
}
