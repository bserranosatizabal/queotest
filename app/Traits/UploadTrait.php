<?php
namespace App\Traits;

use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Image;

trait UploadTrait
{
    public function uploadOne(UploadedFile $uploadedFile, $folder = null, $disk = 'public', $filename = null)
    {
        $name = !is_null($filename) ? $filename : Str::random(25);

        // $resized_image = Image::make($uploadedFile);
        // $resized_image = $resized_image->resize(100,100);

        $image = Image::make($uploadedFile)->fit(100, 100)->stream();
        $file = Storage::disk($disk)->put($folder . $name . $uploadedFile->getClientOriginalExtension(), $image->__toString());

        // $file = $resized_image->storeAs($folder, $name.$uploadedFile->getClientOriginalExtension(), $disk);

        return $file;
    }
}